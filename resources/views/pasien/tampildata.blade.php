<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data sensor</title>
</head>
<body>
    <div class="mt-6 ">
        <h1 class="mt-3">Data Suhu & Kelembaban</h1>
        <table class="mt-6" border="2px" style="text-align:center">
            <tr>
                <th>No</th>
                <th>Suhu</th>
                <th>Kelembaban</th>
                <th>Aksi</th>
            </tr>
            <tr>
                <td>1</td>
                <td>10</td>
                <td>15</td>
                <td>Edit/Hapus</td>
            </tr>
        </table>
    </div>
</body>
</html>